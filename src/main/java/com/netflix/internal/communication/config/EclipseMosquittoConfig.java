package com.netflix.internal.communication.config;

import com.fasterxml.uuid.Generators;
import lombok.extern.log4j.Log4j2;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.endpoint.MessageProducerSupport;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

@Log4j2
@Configuration
public class EclipseMosquittoConfig {
    @Bean
    public MqttPahoClientFactory mqttPahoClientFactory() {
        DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
        MqttConnectOptions options = new MqttConnectOptions();
        options.setServerURIs(new String[]{"tcp://35.199.77.250:80"});
        factory.setConnectionOptions(options);
        return factory;
    }

    @Bean
    public MessageChannel mqttServiceOutputChannel() {
        return new DirectChannel();
    }

    @Bean(name = "mqttServiceOutboundChannel")
    public MessageChannel mqttServiceOutboundChannel() {
        return new DirectChannel();
    }

    @Bean
    @ServiceActivator(inputChannel = "mqttServiceOutboundChannel")
    public MessageHandler mqttServiceOutbound() {
        MqttPahoMessageHandler messageHandler =
                new MqttPahoMessageHandler(Generators.randomBasedGenerator().generate().toString(), mqttPahoClientFactory());
        messageHandler.setAsync(true);
        messageHandler.setDefaultTopic("/communication-service");
        return messageHandler;
    }

    @Bean
    public MessageProducerSupport mqttInbound() {
        MqttPahoMessageDrivenChannelAdapter adapter = new MqttPahoMessageDrivenChannelAdapter(Generators.randomBasedGenerator().generate().toString(),
                mqttPahoClientFactory(), "/communication-service");
        adapter.setCompletionTimeout(5000);
        adapter.setOutputChannel(mqttServiceOutputChannel());
        adapter.setConverter(new DefaultPahoMessageConverter());
        adapter.setQos(0);
        return adapter;
    }


    private LoggingHandler logger() {
        LoggingHandler loggingHandler = new LoggingHandler(LoggingHandler.Level.DEBUG.name());
        loggingHandler.setLoggerName("communication-service");
        return loggingHandler;
    }

    @Bean
    public IntegrationFlow mqttInFlow() {
        return IntegrationFlows.from(mqttInbound())
                .transform(p -> {
                    log.info(p.toString());
                    return p + ", received from MQTT";
                })
                .handle(logger())
                .get();
    }
}
