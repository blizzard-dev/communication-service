package com.netflix.internal.communication.filter;

import com.netflix.internal.communication.dtos.response.UserResponse;
import com.netflix.internal.communication.security.CustomAuthentication;
import com.netflix.internal.communication.service.UserInformationService;
import com.netflix.internal.communication.utils.TokenProvider;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.servlet.FilterChain;
import javax.servlet.GenericFilter;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j2
public class AuthenticationAndAuthorizationFilter extends GenericFilter {
    private static final long serialVersionUID = -1914982310847888377L;

    private UserDetailsService userDetailsService;
    private UserInformationService userInformationService;
    private TokenProvider tokenProvider;

    public AuthenticationAndAuthorizationFilter(UserDetailsService userDetailsService, UserInformationService userInformationService, TokenProvider tokenProvider) {
        this.userDetailsService = userDetailsService;
        this.userInformationService = userInformationService;
        this.tokenProvider = tokenProvider;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        String tokenId = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);
        try {
            if (StringUtils.startsWith(tokenId, "Bearer ")) {
                tokenId = tokenId.substring(7);
                String userName;
                try {
                    userName = tokenProvider.getSubject(tokenId);
                } catch (Exception e) {
                    SecurityContextHolder.clearContext();
                    chain.doFilter(request, response);
                    return;
                }
                UserDetails userDetails = userDetailsService.loadUserByUsername(userName);
                UserResponse userLoginProfile = userInformationService.getUserLoginProfile(userName);
                CustomAuthentication customAuthentication = new CustomAuthentication(userDetails, userDetails.getPassword(), userDetails.getAuthorities(), Long.valueOf(userLoginProfile.getUserId()));
                SecurityContextHolder.getContext().setAuthentication(customAuthentication);
                chain.doFilter(httpServletRequest, httpServletResponse);
            } else {
                chain.doFilter(request, response);
            }
        } finally {
            SecurityContextHolder.clearContext();
        }
    }
}
