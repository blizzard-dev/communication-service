package com.netflix.internal.communication.audit;

import com.netflix.internal.communication.security.CustomAuthentication;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AuditorAwareImpl implements AuditorAware<Long> {
    @Override
    public Optional<Long> getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof CustomAuthentication) {
            return Optional.ofNullable(((CustomAuthentication) authentication).getUserId());
        }
        return Optional.of(Long.MAX_VALUE);
    }
}
