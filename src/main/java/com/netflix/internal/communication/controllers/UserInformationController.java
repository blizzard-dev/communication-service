package com.netflix.internal.communication.controllers;


import com.netflix.internal.communication.constant.Constants;
import com.netflix.internal.communication.dtos.UserRequest;
import com.netflix.internal.communication.dtos.response.UserResponse;
import com.netflix.internal.communication.service.UserInformationService;
import com.netflix.internal.communication.utils.TokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/api/v1/users")
public class UserInformationController {
    @Autowired
    private UserInformationService userInformationService;
    @Autowired
    private TokenProvider tokenProvider;

    @PostMapping(value = "/create")
    public ResponseEntity<UserResponse> createUser(
            @RequestBody UserRequest userRequest,
            @RequestParam(value = "roleName") String roleName) {
        UserResponse user = userInformationService.createUser(userRequest, roleName);
        return ResponseEntity.status(HttpStatus.CREATED).body(user);
    }

    @GetMapping(value = "/profiles")
    public ResponseEntity<UserResponse> getUserLoginProfile(HttpServletRequest httpServletRequest) {
        String token = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);
        if (token != null && token.startsWith(Constants.BEARER_PREFIX)) {
            token = token.substring(7);
        }
        String userName = tokenProvider.getSubject(token);
        UserResponse userLoginProfile = userInformationService.getUserLoginProfile(userName);
        return ResponseEntity.ok().body(userLoginProfile);
    }
}
