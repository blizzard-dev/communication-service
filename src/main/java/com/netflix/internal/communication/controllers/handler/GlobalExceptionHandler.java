package com.netflix.internal.communication.controllers.handler;

import com.netflix.internal.communication.constant.DateTimeConstant;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

@Log4j2
@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(value = Exception.class)
    public ResponseEntity exceptionHandle(Exception e) {
        log.info(e.getLocalizedMessage(), e);
        if (e instanceof BadCredentialsException) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getLocalizedMessage());
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getLocalizedMessage());
        }
    }

    public static void main(String[] args) {
        System.out.println(LocalDateTime.now().format(DateTimeConstant.DEFAULT_DATE_TIME_FORMATTER));
        ZonedDateTime.parse("2020-09-15T00:12:13+0700", DateTimeConstant.DEFAULT_DATE_TIME_FORMATTER);
    }
}
