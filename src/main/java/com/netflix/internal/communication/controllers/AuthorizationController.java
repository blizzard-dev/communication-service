package com.netflix.internal.communication.controllers;

import com.netflix.internal.communication.constant.Constants;
import com.netflix.internal.communication.dtos.LoginDto;
import com.netflix.internal.communication.utils.TokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1")
public class AuthorizationController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private TokenProvider tokenProvider;
    @PostMapping(value = "/authenticate")
    public ResponseEntity<LoginDto> authenticate(@RequestBody LoginDto loginDto) {
        Authentication authentication = new UsernamePasswordAuthenticationToken(loginDto.getUserName(), loginDto.getPassword());
        Authentication authenticate = authenticationManager.authenticate(authentication);
        String token = Constants.BEARER_PREFIX.concat(tokenProvider.generateToken(authenticate.getName()));
        LoginDto response = new LoginDto();
        response.setTokenId(token);
        return ResponseEntity.ok().body(response);
    }
}
