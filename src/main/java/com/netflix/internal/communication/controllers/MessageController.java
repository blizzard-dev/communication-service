package com.netflix.internal.communication.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.netflix.internal.communication.dtos.MessageRequest;
import com.netflix.internal.communication.dtos.response.MessageResponse;
import com.netflix.internal.communication.integration.EclipseMosquittoIntegration;
import com.netflix.internal.communication.service.MessageService;
import com.netflix.internal.communication.utils.ConvertUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/v1")
@RestController
public class MessageController {
    @Autowired
    private MessageService messageService;
    @Autowired
    private EclipseMosquittoIntegration eclipseMosquittoIntegration;

    @PostMapping("/messages/publish")
    public ResponseEntity<MessageResponse> publishMessage(@RequestBody MessageRequest messageRequest) throws JsonProcessingException {
        MessageResponse message = messageService.createMessage(messageRequest);
        eclipseMosquittoIntegration.publish(ConvertUtils.convertObjectToJson(message));
        return ResponseEntity.status(HttpStatus.CREATED).body(message);
    }
}
