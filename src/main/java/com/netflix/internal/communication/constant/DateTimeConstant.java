package com.netflix.internal.communication.constant;

import java.time.format.DateTimeFormatter;

public class DateTimeConstant {
    public static final DateTimeFormatter DEFAULT_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");
}
