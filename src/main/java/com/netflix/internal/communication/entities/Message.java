package com.netflix.internal.communication.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "message")
public class Message extends AuditEntity {
    private static final long serialVersionUID = 504551860247933546L;
    @Id
    @Column(name = "message_id")
    private String messageId;
    @Column(name = "message_content")
    private String messageContent;
    @Column(name = "sender")
    private String sender;
    @Column(name = "reported_date")
    private LocalDateTime reportedDate;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserInformation userInformation;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public UserInformation getUserInformation() {
        return userInformation;
    }

    public void setUserInformation(UserInformation user) {
        this.userInformation = user;
    }

    public LocalDateTime getReportedDate() {
        return reportedDate;
    }

    public void setReportedDate(LocalDateTime reportedDate) {
        this.reportedDate = reportedDate;
    }
}
