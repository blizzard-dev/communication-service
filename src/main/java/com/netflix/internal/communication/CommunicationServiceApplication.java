package com.netflix.internal.communication;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.integration.config.EnableIntegration;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.TimeZone;

@Log4j2
@SpringBootApplication
@EnableIntegration
@EnableConfigurationProperties
@ConfigurationPropertiesScan(basePackages = "com.netflix.internal")
public class CommunicationServiceApplication implements ApplicationRunner {
    @PersistenceContext
    private EntityManager entityManager;

    public static void main(String[] args) {
        SpringApplication.run(CommunicationServiceApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        Query query = entityManager.createNativeQuery("SELECT VERSION()");
        Object singleResult = query.getSingleResult();
        log.info("START CONNECTING TO CLOUD SQL");
        log.info("The version of CLOUD SQL IS {}", singleResult);
    }
}
