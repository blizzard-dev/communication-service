package com.netflix.internal.communication.integration;

import org.springframework.integration.annotation.MessagingGateway;

@MessagingGateway(defaultRequestChannel = "mqttServiceOutboundChannel")
public interface EclipseMosquittoIntegration {
    void publish(String data);
}
