package com.netflix.internal.communication.repository;

import com.netflix.internal.communication.entities.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, String> {

}
