package com.netflix.internal.communication.repository;

import com.netflix.internal.communication.entities.FileUpload;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FileUploadRepository extends JpaRepository<FileUpload, Long> {
    @Query("SELECT fileUpload FROM FileUpload fileUpload INNER JOIN UserInformation user " +
            "ON fileUpload.userInformation.userId = user.userId " +
            "WHERE user.isDeleted = :isDeleted " +
            "AND fileUpload.isDeleted = :isDeleted " +
            "AND user.userId = :userId")
    Page<FileUpload> getFileUploadByUserId(@Param("isDeleted") boolean isDeleted, @Param("userId") Long userId, Pageable pageable);

    @Query("SELECT fileUpload FROM FileUpload fileUpload WHERE fileUpload.isDeleted = :isDeleted " +
            "AND fileUpload.fileName = :fileName")
    List<FileUpload> getFileUploadByFileName(@Param("fileName") String fileName, @Param("isDeleted") boolean isDeleted);
}

