package com.netflix.internal.communication.repository;


import com.netflix.internal.communication.entities.RolePermission;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolePermissionRepository extends JpaRepository<RolePermission, Long> {

}
