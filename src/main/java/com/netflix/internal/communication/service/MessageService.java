package com.netflix.internal.communication.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.netflix.internal.communication.dtos.MessageRequest;
import com.netflix.internal.communication.dtos.response.MessageResponse;

public interface MessageService {
    MessageResponse createMessage(MessageRequest messageRequest);
}
