package com.netflix.internal.communication.service.impl;


import com.fasterxml.uuid.Generators;
import com.netflix.internal.communication.dtos.Pagination;
import com.netflix.internal.communication.dtos.response.FileUploadResponse;
import com.netflix.internal.communication.entities.FileUpload;
import com.netflix.internal.communication.entities.UserInformation;
import com.netflix.internal.communication.repository.FileUploadRepository;
import com.netflix.internal.communication.repository.UserInformationRepository;
import com.netflix.internal.communication.security.CustomAuthentication;
import com.netflix.internal.communication.service.FileUploadService;
import com.netflix.internal.communication.utils.FileStorageLocationUtil;
import org.apache.tomcat.util.http.fileupload.InvalidFileNameException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;

@Service
public class FileUploadServiceImpl implements FileUploadService {
    @Autowired
    private FileUploadRepository fileUploadRepository;
    @Autowired
    private UserInformationRepository userInformationRepository;
    @Autowired
    private FileStorageLocationUtil fileStorageLocationUtil;

    private CustomAuthentication customAuthentication;


    @Override
    public Pagination getFileUploadByUserId(Long userId) {
        return null;
    }

    @Override
    @Transactional
    public FileUploadResponse uploadFile(MultipartFile multipartFile, Long userId) throws IOException {
        customAuthentication = (CustomAuthentication) SecurityContextHolder.getContext().getAuthentication();
        if (!userId.toString().equalsIgnoreCase(customAuthentication.getUserId().toString())) {
            throw new AccessDeniedException(HttpStatus.FORBIDDEN.getReasonPhrase());
        }
        if (multipartFile.getOriginalFilename() != null) {
            FileUpload fileUpload;
            List<FileUpload> fileUploads = fileUploadRepository.getFileUploadByFileName(multipartFile.getOriginalFilename(), Boolean.FALSE);
            if (!fileUploads.isEmpty()) {
                fileUpload = fileUploads.get(0);
                fileUpload.setVersion(fileUpload.getVersion() + 1);
            } else {
                fileUpload = new FileUpload();
                fileUpload.setId(Generators.randomBasedGenerator().generate().toString());
                fileUpload.setExtension(multipartFile.getContentType());
                fileUpload.setFileName(multipartFile.getOriginalFilename());
                fileUpload.setVersion(1);
            }
            Optional<UserInformation> userInformationOptional = userInformationRepository.findUserInformationByUserId(customAuthentication.getUserId(), Boolean.FALSE);
            userInformationOptional.ifPresent(fileUpload::setUserInformation);
            fileUpload = fileUploadRepository.save(fileUpload);

            String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
            if (fileName.contains("..")) {
                throw new InvalidFileNameException(fileName, "Invalid FileName");
            }
            Path targetLocation = this.fileStorageLocationUtil.getFileStorageLocationByUserId(customAuthentication.getUserId().toString()).resolve(fileName);
            Files.copy(multipartFile.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return toFileUploadResponse(fileUpload);
        }
        throw new IllegalArgumentException("Missing filename");
    }

    @Override
    public Resource downloadFile(String fileName, Long userId) throws IOException {
        customAuthentication = (CustomAuthentication) SecurityContextHolder.getContext().getAuthentication();
        List<FileUpload> fileUploads = fileUploadRepository.getFileUploadByFileName(fileName, Boolean.FALSE);
        if (fileUploads.isEmpty()) {
            throw new FileNotFoundException();
        } else {
            FileUpload fileUpload = fileUploads.get(0);
            if (!fileUpload.getUserInformation().getUserId().equals(customAuthentication.getUserId())) {
                throw new AccessDeniedException(HttpStatus.FORBIDDEN.getReasonPhrase());
            } else {
                Path filePath = this.fileStorageLocationUtil.getFileStorageLocationByUserId(customAuthentication.getUserId().toString()).resolve(fileUpload.getFileName()).normalize();
                Resource resource = new UrlResource(filePath.toUri());
                if (resource.exists()) {
                    return resource;
                } else {
                    throw new FileNotFoundException();
                }
            }
        }
    }

    private FileUploadResponse toFileUploadResponse(FileUpload fileUpload) {
        FileUploadResponse fileUploadResponse = new FileUploadResponse();
        fileUploadResponse.setFileId(fileUpload.getId().toString());
        fileUploadResponse.setDescription(fileUpload.getDescription());
        fileUploadResponse.setFileName(fileUpload.getFileName());
        fileUploadResponse.setFilePath(fileUpload.getFilePath());
        fileUploadResponse.setVersion(fileUpload.getVersion().toString());
        fileUploadResponse.setLastModified(fileUpload.getUpdatedOn().toString());
        fileUploadResponse.setExtension(fileUpload.getExtension());
        return fileUploadResponse;
    }
}
