package com.netflix.internal.communication.service;

import com.netflix.internal.communication.dtos.RoleCreateDto;
import com.netflix.internal.communication.dtos.response.RoleResponseDto;

public interface RoleService {
    RoleResponseDto createNewRole(RoleCreateDto roleCreateDto);
}
