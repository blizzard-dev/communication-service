package com.netflix.internal.communication.service;


import com.netflix.internal.communication.dtos.UserRequest;
import com.netflix.internal.communication.dtos.response.UserResponse;

public interface UserInformationService {
    UserResponse createUser(UserRequest userRequest, String roleName);

    UserResponse getUserLoginProfile(String userName);
}
