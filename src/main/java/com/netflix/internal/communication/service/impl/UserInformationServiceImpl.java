package com.netflix.internal.communication.service.impl;

import com.netflix.internal.communication.dtos.UserRequest;
import com.netflix.internal.communication.dtos.response.UserResponse;
import com.netflix.internal.communication.entities.Role;
import com.netflix.internal.communication.entities.UserInformation;
import com.netflix.internal.communication.entities.UserRole;
import com.netflix.internal.communication.entities.UserRoleEmbeddable;
import com.netflix.internal.communication.repository.RoleRepository;
import com.netflix.internal.communication.repository.UserInformationRepository;
import com.netflix.internal.communication.repository.UserRoleRepository;
import com.netflix.internal.communication.service.UserInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserInformationServiceImpl implements UserDetailsService, UserInformationService {

    @Autowired
    private UserInformationRepository userInformationRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) {
        Optional<UserInformation> userInformationOptional = userInformationRepository.findUserInformationByUsername(username, Boolean.FALSE);
        if (userInformationOptional.isPresent()) {
            UserInformation userInformation = userInformationOptional.get();
            return new User(username, userInformation.getPassword(), Collections.emptyList());
        }
        throw new UsernameNotFoundException(username);
    }

    @Transactional
    @Override
    public UserResponse createUser(UserRequest userRequest, String roleName) {
        UserInformation userInformation = toUserInformation(userRequest);
        Optional<Role> employeeRole = roleRepository.findRoleByRoleType(roleName.toUpperCase());
        if (employeeRole.isPresent()) {
            Role role = employeeRole.get();
            userInformation = userInformationRepository.save(userInformation);
            UserRole userRole = new UserRole();
            UserRoleEmbeddable userRoleEmbeddable = new UserRoleEmbeddable();
            userRoleEmbeddable.setRoleId(role.getRoleId());
            userRoleEmbeddable.setUserId(userInformation.getUserId());
            userRole.setUserRoleEmbeddable(userRoleEmbeddable);
            userRole.setRole(role);
            userRole.setUserInformation(userInformation);
            userRoleRepository.save(userRole);
            return toUserResponse(userInformation, Collections.singletonList(role));
        } else {
            throw new EntityNotFoundException();
        }
    }

    @Override
    public UserResponse getUserLoginProfile(String userName) {
        Optional<UserInformation> userInformationOptional = userInformationRepository.findUserInformationByUsername(userName, Boolean.FALSE);
        if (!userInformationOptional.isPresent()) {
            throw new EntityNotFoundException(userName);
        } else {
            UserInformation userInformation = userInformationOptional.get();
            List<UserRole> userRoles = userRoleRepository.findUserRoleByUserId(userInformation.getUserId(), Boolean.FALSE);
            List<Role> roles = new ArrayList<>();
            for (UserRole userRole : userRoles) {
                roles.add(userRole.getRole());
            }
            return toUserResponse(userInformation, roles);
        }

    }

    private UserInformation toUserInformation(UserRequest userRequest) {
        UserInformation userInformation = new UserInformation();
        userInformation.setDateOfBirth(LocalDate.parse(userRequest.getDateOfBirth()));
        userInformation.setFirstName(userRequest.getFirstName());
        userInformation.setLastName(userRequest.getLastName());
        userInformation.setUsername(userRequest.getUserName());
        String password = passwordEncoder.encode(userRequest.getPassword());
        userInformation.setPassword(password);
        userInformation.setGender(userRequest.getGender());
        return userInformation;
    }

    private UserResponse toUserResponse(UserInformation userInformation, List<Role> roles) {
        UserResponse userResponse = new UserResponse();
        userResponse.setDateOfBirth(userInformation.getDateOfBirth().toString());
        userResponse.setUserId(String.valueOf(userInformation.getUserId()));
        userResponse.setFirstName(userInformation.getFirstName());
        userResponse.setUsername(userInformation.getUsername());
        userResponse.setLastName(userInformation.getLastName());
        userResponse.setGender(userInformation.getGender());
        List<String> userRoles = new ArrayList<>();
        for (Role role : roles) {
            userRoles.add(role.getRoleType());
        }
        userResponse.setRoles(userRoles);
        return userResponse;
    }
}
