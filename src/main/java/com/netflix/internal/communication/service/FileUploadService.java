package com.netflix.internal.communication.service;

import com.netflix.internal.communication.dtos.Pagination;
import com.netflix.internal.communication.dtos.response.FileUploadResponse;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileUploadService {
    Pagination getFileUploadByUserId(Long userId);

    FileUploadResponse uploadFile(MultipartFile multipartFile, Long userId) throws IOException;

    Resource downloadFile(String fileName, Long userId) throws IOException;
}
