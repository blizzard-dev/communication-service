package com.netflix.internal.communication.service.impl;

import com.fasterxml.uuid.Generators;
import com.netflix.internal.communication.constant.DateTimeConstant;
import com.netflix.internal.communication.dtos.MessageRequest;
import com.netflix.internal.communication.dtos.response.MessageResponse;
import com.netflix.internal.communication.entities.Message;
import com.netflix.internal.communication.entities.UserInformation;
import com.netflix.internal.communication.repository.MessageRepository;
import com.netflix.internal.communication.repository.UserInformationRepository;
import com.netflix.internal.communication.security.CustomAuthentication;
import com.netflix.internal.communication.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;

@Service
public class MessageServiceImpl implements MessageService {
    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private UserInformationRepository userInformationRepository;

    @Transactional
    @Override
    public MessageResponse createMessage(MessageRequest messageRequest) {
        CustomAuthentication customAuthentication = (CustomAuthentication) SecurityContextHolder.getContext().getAuthentication();
        Message message = new Message();
        message.setMessageId(Generators.randomBasedGenerator().generate().toString());
        message.setMessageContent(messageRequest.getMessageContent());
        message.setReportedDate(LocalDateTime.parse(messageRequest.getReportedDate(), DateTimeConstant.DEFAULT_DATE_TIME_FORMATTER));
        message.setSender(customAuthentication.getName());
        Optional<UserInformation> optional = userInformationRepository.findUserInformationByUsername(customAuthentication.getName(), false);
        if (optional.isPresent()) {
            UserInformation userInformation = optional.get();
            message.setUserInformation(userInformation);
        }
        message = messageRepository.save(message);
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setMessageContent(message.getMessageContent());
        messageResponse.setMessageId(message.getMessageId());
        messageResponse.setReportedDate(message.getReportedDate().atZone(ZoneId.of("UTC")).format(DateTimeConstant.DEFAULT_DATE_TIME_FORMATTER));
        messageResponse.setSender(message.getSender());
        return messageResponse;
    }
}
